<!DOCTYPE html>
<html>
  <head>
    <title>Projet IPS-Prod - Local density of a nuclear system</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="core/fonts/mono.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/animate.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/style_core.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/mermaid.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/gitgraph.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/style_ensiie.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/katex.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/asciinema-player.css"> 



  </head>
  <body>
    <textarea id="source" readonly>
class: titlepage

.title[
Projet IPS-Prod - [Local density of a nuclear system](https://dubrayn.github.io/IPS-PROD/project.html#1)
]

.subtitle[
L. Mallardeau, V. Gagliano, G. Mézière - ENSIIE - 2020
]

.row[
`$$ $$`
]

.footnote[
[:book:](index.html)
]

???

Some slide notes.

Some math can be put inline `$a = \sqrt{b^2+c^2}$` or as

`$$a_{0}=\frac{1}{4}b_{1}$$`

Some items:
* first
* second
* third

---
layout: true
class: animated fadeIn middle numbers

.footnote[
L. Mallardeau, V. Gagliano, G. Mézière - ENSIIE - 2020 - [:book:](index.html)
]

---
# Objectifs du projet

* Calculer la densité locale atomique  `$\rho(r)$`
* Optimiser l'algorithme
* Tracer le résultat en 2D et en 3D

---
class: toc
#Sommaire


1. Introduction : problème physique

2. Description générale du projet
    - Chaîne de compilation
    - Optimisation de l'algorithme naïf
    - Conversion de coordonnées

3. Tests unitaires et documentation

4. Résultats

---

# I - Introduction


.hcenter.w85[
        ![](images/local_density.PNG)
        Densité locale de protons et de neutrons dans un noyau d'atome de néon
]
‌.hcenter[
`$$\rho(\mathbf{r}) \equiv \sum_a \sum_b \rho_{ab}\psi_a(\mathbf{r})\psi^*_b(\mathbf{r})$$`]




---
# II - Chaîne de compilation : Makefile


```Makefile
CC = g++
CFLAGS = -g -Wall -Wextra -larmadillo -lm
OBJECTS = obj/MathTools.o obj/Poly.o obj/Basis.o obj/NaiveRho.o obj/OptimizedRho.o
TESTS = tests/TestMandatory00 tests/TestMandatory01 tests/TestMandatory02 tests/TestMandatory03 tests/CompareMethods
TARGET = main

all: $(TARGET)

$(TARGET): $(TARGET).cpp $(OBJECTS)
	$(CC) $(CFLAGS) $^ -o $@

obj/%.o: src/%.cpp headers/%.h
	@if [ ! -d obj ]; then mkdir obj; fi
	$(CC) $(CFLAGS) -c $< -o $@


tests/%.cpp: tests/%.h
	cxxtestgen --error-printer $^ -o $@

tests/%: tests/%.cpp $(OBJECTS)
	$(CC) $(CFLAGS) -o $@ $^

tests: $(TESTS) $(OBJECTS)


.PHONY: doc
doc:
	doxygen Doxyfile

.PHONY: clean re
clean:
	rm -f $(OBJECTS) $(TARGET) *.out
	rm -f $(TESTS) tests/*.cpp

re: clean all
```




---
# II - Code : NaiveRho.cpp


```C++
#include "../headers/NaiveRho.h"
#include "../headers/Basis.h"

arma::mat NaiveRho::density(arma::vec zVals, arma::vec rVals) {
    arma::mat rho;
    rho.load("rho.arma", arma::arma_ascii);
    arma::mat result = arma::zeros(zVals.size(), rVals.size()); // number of points on r- and z- axes
    Basis basis(1.935801664793151,      2.829683956491218,     14,     1.3);
    int i = 0;
    int j = 0;
    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                for (int mp = 0; mp < basis.mMax; mp++)
                {
                    for (int np = 0; np < basis.nMax(mp); np++)
                    {
                        for (int n_zp = 0; n_zp < basis.n_zMax(mp, np); n_zp++)
                        {
                            arma::mat funcA = basis.basisFunc( m,  n,  n_z, zVals, rVals);
                            arma::mat funcB = basis.basisFunc(mp, np, n_zp, zVals, rVals);
                            result += funcA % funcB * rho(i, j);
                            i++;
                        }
                    }
                }
                j++;
                i = 0;
            }
        }
    }
    return result;
}
```

```C++
arma::mat Basis::basisFunc(int m, int n, int n_z, arma::vec zVals, arma::vec rVals) {
    return zPart(zVals, n_z) * (rPart(rVals, m, n).t());
}
```







---

# II - Optimisation 1

.row.hcenter[

```C++
int i = 0;
    arma::icube index(basis.mMax, basis.nMax(0), basis.n_zMax(0, 0), arma::fill::zeros);
    for (int m = 0; m < basis.mMax; ++m)
    {
        for (int n = 0; n < basis.nMax(m); ++n)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); ++n_z)
            {
                index(m, n, n_z) = i;
                i++;
            }
        }
    }
```
]
.column.hcenter.w40[![](images/OptimizationStep1Result.png)]






---

# II - Optimisation 2

.row.hcenter[
```C++
// Since rho(a, b) = 0 if m_a != m_b, we can remove the for (int m_b = 0;...) loop
for (int n_b = 0; n_b < basis.nMax(m_a); n_b++)
{
    for (int n_z_b = 0; n_z_b < basis.n_zMax(m_a, n_b); n_z_b++) 
    {
        arma::mat funcA = basis.basisFunc(m_a, n_a, n_z_a, zVals, rVals);
        arma::mat funcB = basis.basisFunc(m_a, n_b, n_z_b, zVals, rVals);
        result += funcA % funcB * rho(index(m_a, n_a, n_z_a), index(m_a, n_b, n_z_b));
    }
}
```
]
.column.hcenter.w40[![](images/OptimizationStep2Result.png)]



---

# II - Optimisation 3
```C++
arma::vec zPart_a;
arma::vec rPart_a;
arma::vec zPart_b;
arma::vec rPart_b;
for (int m_a = 0; m_a < basis.mMax; m_a++)
{
    for (int n_a = 0; n_a < basis.nMax(m_a); n_a++)
    {
        rPart_a = basis.rPart(rVals, m_a, n_a);
        for (int n_z_a = 0; n_z_a < basis.n_zMax(m_a, n_a); n_z_a++)
        {
            zPart_a = basis.zPart(zVals, n_z_a);
            // Since rho(a, b) = 0 if m_a != m_b, we can remove the for (int m_b = 0;...) loop
            for (int n_b = 0; n_b < basis.nMax(m_a); n_b++)
            {
                rPart_b = basis.rPart(rVals, m_a, n_b);
                for (int n_z_b = 0; n_z_b < basis.n_zMax(m_a, n_b); n_z_b++) {
                    zPart_b = basis.zPart(zVals, n_z_b);
                    arma::mat funcA = zPart_a * rPart_a.t();
                    arma::mat funcB = zPart_b * rPart_b.t();
                    result += funcA % funcB * rho(index(m_a, n_a, n_z_a), index(m_a, n_b, n_z_b));
                }
            }
        }
    }
}
```
.column.hcenter.w40[![](images/OptimizationStep3Result.png)]


---

# II - Optimisation 4
```C++
arma::mat zParts(zVals.n_elem, basis.n_zMax(0, 0));
    for (int n_z = 0; n_z < basis.n_zMax(0, 0); ++n_z) 
    {
        zParts.col(n_z) = basis.zPart(zVals, n_z);
    }
```
.column.hcenter.w40[![](images/OptimizationStep4Result.png)]


---

# II - Optimisation 5
```C++
int i = 0;
    arma::mat zParts(zVals.n_elem, basis.n_zMax(0, 0));
    arma::icube index(basis.mMax, basis.nMax(0), basis.n_zMax(0, 0), arma::fill::zeros);
    for (int m = 0; m < basis.mMax; ++m) {
        for (int n = 0; n < basis.nMax(m); ++n) {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); ++n_z) {
                if (m == 0 && n == 0) {
                    zParts.col(n_z) = basis.zPart(zVals, n_z);
                }
                index(m, n, n_z) = i;
                i++;
            }
        }
    }
```
.column.hcenter.w40[![](images/OptimizationStep5Result.png)]

---
# II - Optimisation : algorithme final

```C++
arma::mat OptimizedRho::density(arma::vec zVals, arma::vec rVals) {
    arma::mat rho;
    rho.load("rho.arma", arma::arma_ascii);
    arma::mat result = arma::zeros(zVals.size(), rVals.size()); // number of points on r- and z- axes
    Basis basis(1.935801664793151, 2.829683956491218, 14, 1.3);

    int i = 0;
    arma::mat zParts(zVals.n_elem, basis.n_zMax(0, 0));
    arma::icube index(basis.mMax, basis.nMax(0), basis.n_zMax(0, 0), arma::fill::zeros);
    for (int m = 0; m < basis.mMax; ++m) {
        for (int n = 0; n < basis.nMax(m); ++n) {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); ++n_z) {
                if (m == 0 && n == 0) {
                    zParts.col(n_z) = basis.zPart(zVals, n_z);
                }
                index(m, n, n_z) = i;
                i++;
            }
        }
    }

    arma::vec rPart_a;
    arma::vec rPart_b;
    for (int m_a = 0; m_a < basis.mMax; m_a++)
    {
        for (int n_a = 0; n_a < basis.nMax(m_a); n_a++)
        {
            rPart_a = basis.rPart(rVals, m_a, n_a);
            for (int n_z_a = 0; n_z_a < basis.n_zMax(m_a, n_a); n_z_a++)
            {
                arma::mat funcA = zParts.col(n_z_a) * rPart_a.t();
                for (int n_b = 0; n_b < basis.nMax(m_a); n_b++)
                {
                    rPart_b = basis.rPart(rVals, m_a, n_b);
                    for (int n_z_b = 0; n_z_b < basis.n_zMax(m_a, n_b); n_z_b++) {
                        arma::mat funcB = zParts.col(n_z_b) * rPart_b.t();
                        result += funcA % funcB * rho(index(m_a, n_a, n_z_a), index(m_a, n_b, n_z_b));
                    }
                }
            }
        }
    }
    return result;
}
```

---
# II - Conversion de coordonnées

```C++
arma::vec zVals = arma::linspace(-20, 20, 64);
arma::vec rVals = arma::linspace(0, 10, 16);
arma::cube out = convert_coordinates(OptimizedRho::density(zVals, rVals));
```


```C++
arma::cube convert_coordinates(arma::mat input) {
    int nbp_r = input.n_cols;
    int nbp_z = input.n_rows;
    int x_min = -10, x_max = 10, nbp_x = 32;
    int y_min = -10, y_max = 10, nbp_y = 32;

    arma::cube output = arma::cube(nbp_x, nbp_y, nbp_z).zeros();
    for (int x = 0; x < nbp_x; x++) {
        for (int y = 0; y < nbp_y; y++) {
            double real_x = x_min + ((double) x/nbp_x)*(x_max - x_min);
            double real_y = y_min + ((double) y/nbp_y)*(y_max - y_min);
            double real_r = sqrt(pow(real_x, 2) + pow(real_y, 2));
            int r = (real_r/sqrt(x_max*x_max + y_max*y_max)) * nbp_r;
            if (r < nbp_r)
                output.tube(x, y) = input.col(r);
        }
    }
    return output;
}
```


---

# III - Tests unitaires

```C++
class TestMandatory00 : public CxxTest::TestSuite {
    public:
    void testPoly(void) {
        // Mandatory test #00 - Hermite and Laguerre polynomials
        Poly poly;
        arma::vec zVals, calcVals, targetVals;
        zVals = {-3.1, -2.3, -1.0, -0.3, 0.1, 4.3, 9.2, 13.7};
        poly.calcHermite(6, zVals); // compute Hermite polynomials for n in {0 ... 5}
        calcVals   = poly.hermite(4); // n = 4
        targetVals = {  1.02835360e+03,  2.05825600e+02, -2.00000000e+01,  7.80960000e+00,
                        1.15216000e+01,  4.59456160e+03,  1.10572154e+05,  5.54643458e+05};
        TS_ASSERT_DELTA(arma::norm(calcVals / targetVals - 1.0), 0.0, 1e-08);
        calcVals   = poly.hermite(5); // n = 5
        targetVals = { -4.76676832e+03, -3.88909760e+02,  8.00000000e+00, -3.17577600e+01,
                       1.18403200e+01,  3.48375818e+04,  1.98557479e+06,  1.50339793e+07};
        TS_ASSERT_DELTA(arma::norm(calcVals / targetVals - 1.0), 0.0, 1e-08);
        zVals = {0.1, 0.3, 1.2, 1.8, 2.0, 2.5, 7.1, 11.1};
        poly.calcLaguerre(6, 4, zVals); // compute generalized Laguerre polynomials for m in {0 ... 5} and n in {0 ... 3}
        calcVals   = poly.laguerre(4, 2); // m = 4, n = 2
        targetVals = {  14.405,  13.245,  8.52 ,  5.82 ,  5.,  3.125,  -2.395,  10.005};
        TS_ASSERT_DELTA(arma::norm(calcVals / targetVals - 1.0), 0.0, 1e-08);
        calcVals   = poly.laguerre(5, 3); // m = 5, n = 3
        targetVals = { 53.23983333,  47.95550000,  27.87200000,  17.5880,
                       14.66666667,   8.39583333,  -0.81183333,  10.1015};
        TS_ASSERT_DELTA(arma::norm(calcVals / targetVals - 1.0), 0.0, 1e-08);
    }
};
```

.column.hcenter.w80[![](images/unit_tests.PNG)]


---

# III - Documentation
.hcenter.w100[![](images/Doxygen.png)]

.hcenter[Documentation Doxygen]

---

# IV - Résultat

.hcenter[![](images/2dplot.png)]

.hcenter[Plot 2D du résultat pour $y=0$]

---

# IV - Résultat

.hcenter.w60[![](images/3dplot.png)]

.hcenter[Plot 3D du résultat]

‌


    </textarea>


    <script src="core/javascript/remark.js"></script>
    <script src="core/javascript/katex.min.js"></script>
    <script src="core/javascript/auto-render.min.js"></script>
    <script src="core/javascript/emojify.js"></script>
    <script src="core/javascript/mermaid.js"></script>
    <script src="core/javascript/jquery-2.1.1.min.js"></script>
    <script src="core/javascript/extend-jquery.js"></script>
    <script src="core/javascript/gitgraph.js"></script>
    <script src="core/javascript/plotly.js"></script>
    <script src="core/javascript/asciinema-player.js"></script>
    <script src="core/javascript/bokeh-2.2.1.min.js"></script>
    <script src="core/javascript/bokeh-widgets-2.2.1.min.js"></script>
    <script src="core/javascript/bokeh-tables-2.2.1.min.js"></script>
    <script src="core/javascript/bokeh-api-2.2.1.min.js"></script>

    <script>

    // === Remark.js initialization ===
    var slideshow = remark.create(
    {
      highlightStyle: 'monokai',
      countIncrementalSlides: false,
      highlightLines: false
    });

    // === Mermaid.js initialization ===
    mermaid.initialize({
      startOnLoad: false,
      cloneCssStyles: false,
      flowchart:{
        height: 50
      },
      sequenceDiagram:{
        width: 110,
        height: 30
      }
    });

    function initMermaid(s) {
      var diagrams = document.querySelectorAll('.mermaid');
      var i;
      for(i=0;i<diagrams.length;i++){
        if(diagrams[i].offsetWidth>0){
          mermaid.init(undefined, diagrams[i]);
        }
      }
    }

    slideshow.on('afterShowSlide', initMermaid);
    initMermaid(slideshow.getSlides()[slideshow.getCurrentSlideIndex()]);


    // === Emojify.js initialization ===
    emojify.run();

    // KaTeX
    renderMathInElement(document.body,{delimiters: [{left: "$$", right: "$$", display: true}, {left: "$", right: "$", display: false}], ignoredTags: ["script", "noscript", "style", "textarea", "pre"] });


    </script>
    <script src="gitgraphs.js" type="text/javascript"></script>

  </body>
</html>


